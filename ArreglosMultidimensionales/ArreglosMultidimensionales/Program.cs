﻿using System;

namespace ArreglosMultidimensionales
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] matriz = new int[2, 4];


            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine("ingresa un numero");
                    int num = Convert.ToInt32(Console.ReadLine());
                    matriz[i, j] = num;
                    Console.WriteLine(i + "," + j);
                }
            }
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine(matriz[i, j]);
                }
            }
            Console.ReadKey();
        }
    }
}
