﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1U5InvaliedCasException
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {

                int num1 = int.Parse(txtn1.Text);
                int num2 = int.Parse(txtn2.Text);
                double Total;
                Total = (num1 / num2);
                MessageBox.Show ( "La division es:" + Total);

            }
            catch (IndexOutOfRangeException ex)
            {
                MessageBox.Show(ex.Message);

            }
            catch(DivideByZeroException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblRes_Click(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {

            try
            {
                Close();


            }
            catch (InvalidCastException ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int num1 = int.Parse(txtn1.Text);
            int num2 = int.Parse(txtn2.Text);
            double Total;
            Total = (num1 * num2);
            MessageBox.Show("La multiplicacion es:" + Total);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtn1.Text = "";
            txtn2.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
