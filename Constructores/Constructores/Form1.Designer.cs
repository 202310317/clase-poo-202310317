﻿
namespace Constructores
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxmultas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblresultado = new System.Windows.Forms.Label();
            this.btncalcularmulta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbxmultas
            // 
            this.cbxmultas.FormattingEnabled = true;
            this.cbxmultas.Items.AddRange(new object[] {
            "Mal estacionamiento ",
            "Exceso de Velocidad",
            "Pasarse un alto",
            "Exceso de Alcohol"});
            this.cbxmultas.Location = new System.Drawing.Point(319, 76);
            this.cbxmultas.Name = "cbxmultas";
            this.cbxmultas.Size = new System.Drawing.Size(121, 21);
            this.cbxmultas.TabIndex = 0;
            this.cbxmultas.SelectedIndexChanged += new System.EventHandler(this.cbxmultas_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(143, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Selecciona Multa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(193, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pago De La Multa";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblresultado
            // 
            this.lblresultado.AutoSize = true;
            this.lblresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblresultado.Location = new System.Drawing.Point(424, 225);
            this.lblresultado.Name = "lblresultado";
            this.lblresultado.Size = new System.Drawing.Size(16, 16);
            this.lblresultado.TabIndex = 3;
            this.lblresultado.Text = "..";
            // 
            // btncalcularmulta
            // 
            this.btncalcularmulta.BackColor = System.Drawing.Color.Red;
            this.btncalcularmulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncalcularmulta.ForeColor = System.Drawing.Color.Aqua;
            this.btncalcularmulta.Location = new System.Drawing.Point(364, 297);
            this.btncalcularmulta.Name = "btncalcularmulta";
            this.btncalcularmulta.Size = new System.Drawing.Size(134, 32);
            this.btncalcularmulta.TabIndex = 4;
            this.btncalcularmulta.Text = "Calcular Multa";
            this.btncalcularmulta.UseVisualStyleBackColor = false;
            this.btncalcularmulta.Click += new System.EventHandler(this.btncalcularmulta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btncalcularmulta);
            this.Controls.Add(this.lblresultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxmultas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxmultas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblresultado;
        private System.Windows.Forms.Button btncalcularmulta;
    }
}

