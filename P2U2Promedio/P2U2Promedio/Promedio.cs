﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P2U2Promedio
{
    public class Promedio
    {
        private int Cal1;
        private int Cal2;
        private int Cal3;
        private double promedio;
        public void CalPromedio()
        {
            Console.WriteLine("ingresa Calificacion1");
            Cal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("ingresa Calificacion2");
            Cal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("ingresa Calificacion3");
            Cal3 = Convert.ToInt32(Console.ReadLine());
            promedio = (Cal1 + Cal2 + Cal3) / 3;
        }

        public void MostrarPromedio()
        {
            Console.WriteLine("El promedio obtenido es {0}", promedio);
        }
    }
}