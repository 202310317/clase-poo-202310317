﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OperacionesAritmeticas
{
    class OperacionesAritmeticas
    {

        public double suma(double a, double b)
        {
            return a + b;
        }
        public double resta(double a, double b)
        {
            return a - b;
        }
        public double multiplica(double a, double b)
        {
            return a * b;
        }
        public double Divide(double a, double b)
        {
            return a / b;
        }
    }
}
