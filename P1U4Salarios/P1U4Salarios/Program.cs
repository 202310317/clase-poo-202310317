﻿using System;

namespace P1U4Salarios
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bienvenido Empresa “Seleny” S.A. de C.V ");
            Console.WriteLine("");
            Console.WriteLine("");

            Intendente obj = new Intendente();
            obj.salario1();
            Console.WriteLine("");

            Produccion obj2 = new Produccion();
            obj2.salario2();
            Console.WriteLine("");

            Administrativos obj3 = new Administrativos();
            obj3.salario3();
            Console.WriteLine("");

            Gerencia obj4 = new Gerencia();
            obj4.salario4();
            Console.WriteLine("");

            Dueños obj5 = new Dueños();
            obj5.salario5();
            Console.WriteLine("");

            Console.WriteLine("Gracias por su paciencia...");
            Console.ReadKey();


          
        }
    }
}
