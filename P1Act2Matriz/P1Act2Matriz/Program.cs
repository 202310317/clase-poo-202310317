﻿using System;

namespace P1Act2Matriz
{
    class Program
    {
        static void Main(string[] args)
        {
            Operaciones op = new Operaciones();
            op.ImprimeMatriz();
            Console.WriteLine("");
            Console.WriteLine("");
            op.LeerMatriz();
            Console.ReadKey();
        }
    }
}
