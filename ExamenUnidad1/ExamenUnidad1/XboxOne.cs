﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenUnidad1
{
    class XboxOne
    {

        private int accesorios = 0;
        private int Almacenamiento = 0;
        private string color = "";
        private int año = 0;
        private int precio = 0;
        private string tipo = "";


        public void setaccesorios(int accesorios)
        {
            this.accesorios = accesorios;
        }

        public int getaccesorios()
        {
            return this.accesorios;
        }
        public void setalmacenamiento(int almacenamiento)
        {
            this.Almacenamiento=almacenamiento;
        }

        public int getalmacenamiento()
        {
            return this.Almacenamiento;
        }
        public void setcolor(string color)
        {
            this.color = color;
        }
        public string getcolor()
        {
            return this.color;
        }
        public void setaño(int año)
        {
            this.año = año;
        }
        public int getaño()
        {
            return this.año;
        }
        public void setprecio(int precio)
        {
            this.precio = precio;

        }
        public int getprecio()
        {
            return this.precio;
        }
        public void settipo(string tipo)
        {
            this.tipo = tipo;

        }
       public string gettipo()
        {
            return this.tipo;
        }
    }
}

