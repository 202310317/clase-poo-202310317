﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenUnidad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XboxOne xbox = new XboxOne();

            xbox.setaccesorios(4);
            int accesoriosxbox = xbox.getaccesorios();

            xbox.setalmacenamiento(500);
            int almacenamientoxbox = xbox.getalmacenamiento();

            xbox.setcolor("negro");
            string colorxbox = xbox.getcolor();

            xbox.setaño(2020);
            int añoxbox = xbox.getaño();

            xbox.setprecio(10000);
            int precioxbox = xbox.getprecio();

            xbox.settipo("xbox one series");
            string tipoxbox = xbox.gettipo();

            MessageBox.Show("los accesorios incluidos son  " + accesoriosxbox.ToString());

            MessageBox.Show("el almacenamiento del xbox es  "+ almacenamientoxbox.ToString());

            MessageBox.Show("el color del xbox seleccionado es  " + colorxbox.ToString());

            MessageBox.Show("el año del xbox es  " + añoxbox.ToString());

            MessageBox.Show("el precio del xbox seleccionado es  " + precioxbox.ToString());

            MessageBox.Show("Tipo de xbox seleccionado  " + tipoxbox.ToString());

         
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
