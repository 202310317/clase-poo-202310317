﻿using System;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //aqui mandamos a llamar a la clase hijo que en este caso la llamaremos obj y los metodos heredados de la clase Herencia
            //nos permitira llamarlos desde esta clase
            Hijo obj = new Hijo();

            obj.Metodo1();
            obj.AccesoMetodo3();


            //aqui llamaremos la clase Herencia (ob2) para poder llamar el metodo 2 ya que esta en private y es la unica que puede mandarlo a llamar
            Herencia ob2 = new Herencia();
            ob2.AccesoMetodo2();
            Console.ReadKey();
        }
    }
}
