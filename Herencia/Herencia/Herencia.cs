﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Herencia
{

    //Empezamos el programa declarando la clase Herencia
    class Herencia
    {
        //declaramos sus 3 atributos enteros, con los modificadores de acceso, public,private y protected
        public int atributo1;
        private int atributo2;
        protected int atributo3;

        // empezamos a declarar sus metodos y empezamos con el publico
        public void Metodo1()
        {
            Console.WriteLine("este es el metodo 1 de Herencia");
        }
        //el metodo 2 es distinto al anterior porque no puede acceder nadie mas que la misma clase
        private void Metodo2()
        {
            Console.WriteLine("este metodo no se puede acceder desde herencia");
        }
        //el metodo 3 lo declaramos de tipo protected este se utiliza para heredar
        protected void Metodo3()
        {
            Console.WriteLine("este es el medoto 3 de Herencia Protected");
        }

        //como lo hablamos anterior mente no se puede acceder al metodo 2 en herencia solo la class que lo declaro por ello lo llamamos
        //en esta misma clase
        public void AccesoMetodo2()
        {
            Metodo2();

        }
    }

    //despues declaramos la clase Hijo que es la que va a heredar todo lo de la clase Herencia en este caso la llamaremos Madre
    class Hijo: Herencia
    {
        //en esta parte la clase hijo manda a llamar el metodo 3 por herencia y por ello no nos marca ningun herror
        public void AccesoMetodo3()
        {
            Metodo3();
        }

    }
}
