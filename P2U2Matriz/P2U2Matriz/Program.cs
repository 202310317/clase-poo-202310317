﻿using System;

namespace P2U2Matriz
{
    class Program
    {
        static void Main(string[] args)
        {
            Matriz mat = new Matriz();
            mat.LeeDatos();
            mat.MostrarDatos();

            Console.ReadKey();
        }
    }
}
