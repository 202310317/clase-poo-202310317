﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            
           int a = int.Parse(txta.Text);
            int b = int.Parse(txtb.Text);
            int c = 0; 
            try
            {
                Ejemplo ejem = new Ejemplo();
                c = ejem.CalculaDivision(a, b);
                

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
                
            }
            finally
            {
                
                MessageBox.Show ("El resultado es: " + a+"/"+b+"="+c);
            }


        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txta.Text = " ";
            txtb.Text = " ";
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
