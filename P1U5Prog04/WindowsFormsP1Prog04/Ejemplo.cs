﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsP1Prog04
{
    class Ejemplo
    {
        public int CalculaDivision(int numerador, int denominador)
        {
            if (denominador == 0)
                throw new Exception("El denominador NO debe ser cero");
            else
                return (numerador / denominador);
        }
    }
}
