﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_2
{
    class tv
    {
        private int tamanio = 0;
        private int volumen = 0;
        private string color = "";
        private int brillo = 0;
        private int contraste = 0;
        private string marca = "";

        
        public void settamanio(int tamanio )
        {
            this.tamanio = tamanio;
        }

        public int gettamanio()
        {
            return this.tamanio;
        }
        public void setvolumen(int volumen)
        {
            this.volumen = volumen;
        }
        
        public int getvolumen()
        {
            return this.volumen;
        }
        public void setcolor(string color)
        {
            this.color = color;
        }
        public string getcolor()
        {
            return this.color;
        }
        public void setbrillo(int brillo)
        {
            this.brillo = brillo;
        }
        public int getbrillo()
        {
            return this.brillo;
        }
        public void setcontraste(int contraste)
        {
            this.contraste = contraste;

        }
        public int getcontraste()
        {
            return this.contraste;
        }
        public void setmarca(string marca)
        {
            this.marca = marca;

        }
        public string getmarca()
        {
            return this.marca;
        }
    }
}
