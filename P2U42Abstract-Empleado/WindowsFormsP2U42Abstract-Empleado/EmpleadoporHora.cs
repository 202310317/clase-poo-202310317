﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U42Abstract_Empleado
{
    public class EmpleadoporHora : Empleado
    {
        public override void CalculaSalario(double SA, double SH, int HT, double TC,int VB, double EB)
        {
            if (HT <= 40)
            {
                Sueldo = SH * HT;
            }

            if (HT >40)
            {
                Sueldo = 40 *SH+(HT-40)*1.5;
            }
        }

        internal void CalculaSalario(int v, double sueldoHora, double horasTrabajadas)
        {
            if (horasTrabajadas <= 40)
            {
                Sueldo = sueldoHora * horasTrabajadas;
            }

            if (horasTrabajadas > 40)
            {
                Sueldo = 40 * sueldoHora + (horasTrabajadas - 40) * 1.5;
            }
        }
    }
}