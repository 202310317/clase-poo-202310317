﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsP2U42Abstract_Empleado
{
    public class EmpleadoBaseMasComision : Empleado
    {
        public override void CalculaSalario(double SA, double SH, int HT, double TC, int VB, double EB)
        {
            sueldoBase = TC * VB + EB;
        }

        internal void CalculaSalario(int v, double tC, double vB, TextBox txtSalarioBase)
        {
            sueldoBase = tC * v + vB;
        }
    }
}