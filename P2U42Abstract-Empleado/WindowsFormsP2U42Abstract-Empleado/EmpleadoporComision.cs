﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U42Abstract_Empleado
{
    public class EmpleadoporComision : Empleado
    {
        public override void CalculaSalario(double SA, double SH, int HT, double TC, int VB, double EB)
        {
            ventasBrutas = TC * VB;
        }

        internal void CalculaSalario(int v, double tarifaComision, double ventasBrutas)
        {
            ventasBrutas = tarifaComision * v;
        }
    }
}