﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP2U42Abstract_Empleado
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            lstDato.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double salario = double.Parse(txtSalario.Text);
            EmpleadoAsalariado ObjEA = new EmpleadoAsalariado();
            ObjEA.CalculaSalario(salario,0,0);
            lstDato.Items.Add("Nombre Empleado:"+Nombre);
            lstDato.Items.Add("Apellido Empleado:" + Apellido);
            lstDato.Items.Add("Numero Seguro Social:" + NSS);
            lstDato.Items.Add("Sueldo Semanal:" + ObjEA.Sueldo);
        }

       
        private void btnEmpleadoHora_Click(object sender, EventArgs e)
        {
            lstDato.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double SueldoHora = double.Parse(txtSueldoHora.Text);
            double HorasTrabajadas = double.Parse(txtHorasTrabajadas.Text);
            EmpleadoporHora ObjEH = new EmpleadoporHora();
            ObjEH.CalculaSalario(0,SueldoHora,HorasTrabajadas);
            lstDato.Items.Add("Nombre Empleado:" + Nombre);
            lstDato.Items.Add("Apellido Empleado:" + Apellido);
            lstDato.Items.Add("Numero Seguro Social:" + NSS);
            lstDato.Items.Add("Sueldo Semanal:" + ObjEH.Sueldo);
        }

        private void btnEmpleadoporComision_Click(object sender, EventArgs e)
        {
            lstDato.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double TarifaComision = double.Parse(txtTarifaComision.Text);
            double VentasBrutas = int.Parse(txtVentasBrutas.Text);
            EmpleadoporComision ObjEC = new EmpleadoporComision();
            ObjEC.CalculaSalario(0,TarifaComision,VentasBrutas);
            lstDato.Items.Add("Nombre Empleado:" + Nombre);
            lstDato.Items.Add("Apellido Empleado:" + Apellido);
            lstDato.Items.Add("Numero Seguro Social:" + NSS);
            lstDato.Items.Add("Sueldo Semanal:" + ObjEC.Sueldo);
        }

        private void btnEmpleadoBaseComision_Click(object sender, EventArgs e)
        {
            lstDato.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double TC = double.Parse(txtTC.Text);
            double VB = double.Parse(txtVB.Text);
            double SalarioBase = double.Parse(txtSalarioBase.Text);
            EmpleadoBaseMasComision ObjEBMC =new EmpleadoBaseMasComision();
            ObjEBMC.CalculaSalario(0,TC, VB, txtSalarioBase);
            lstDato.Items.Add("Nombre Empleado:" + Nombre);
            lstDato.Items.Add("Apellido Empleado:" + Apellido);
            lstDato.Items.Add("Numero Seguro Social:" + NSS);
            lstDato.Items.Add("Sueldo Semanal:" + ObjEBMC.Sueldo);

        }
    }
}
