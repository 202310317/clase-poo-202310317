﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U42Abstract_Empleado
{
    public abstract class Empleado
    {
        public double Sueldo;
        public double sueldoBase;
        public double sueldoPorHoras;
        public double ventasBrutas;
        public double salarioSemanal;

        public abstract void CalculaSalario(double SA,double SH, int HT,double TC, int VB, double EB);
        //SA sueldo empleado Asalariado
        //SH sueldo hora Empleado por horas
        //HT horas trabajadas de empleado por horas
        //TC tarifa Comision
       //VB Ventas Brutas
       //EB empleado Base
    }
}