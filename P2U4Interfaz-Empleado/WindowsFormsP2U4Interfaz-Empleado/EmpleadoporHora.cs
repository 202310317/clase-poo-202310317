﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U4Interfaz_Empleado
{
    public class EmpleadoporHora : Empleado
    {
        public int HT;
        public double SH;
        public double sueldo;

        public string Sueldo { get; internal set; }

        public void CalculaSalario(double SA, double SH, int HT, double TC, int VB, double EB)
        {
            if (HT <= 40)
            {
                sueldo = SH * HT;
            }

            if (HT > 40)
            {
                sueldo = 40 * SH + (HT - 40) * 1.5;
            }
        }

        internal void CalculaSalario(double sueldo, double SH, int HT)
        {
            if (HT <= 40)
            {
                sueldo = SH * HT;
            }

            if (HT > 40)
            {
                sueldo = 40 * SH + (HT - 40) * 1.5;
            }
        }
    }
}