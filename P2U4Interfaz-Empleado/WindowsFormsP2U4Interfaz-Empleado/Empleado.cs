﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U4Interfaz_Empleado
{
    public interface Empleado
    {
        void CalculaSalario(double SA, double SH, int HT, double TC, int VB, double EB);
    }
}