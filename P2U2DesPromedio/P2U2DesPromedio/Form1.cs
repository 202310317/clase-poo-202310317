﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2U2DesPromedio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            metodo();

        }
        public void metodo()
        {
            double Cal1 = Convert.ToDouble(MATEMATICAS.Text);
            double Cal2 = Convert.ToDouble(ESPAÑOL.Text);
            double Cal3 = Convert.ToDouble(CIENCIAS.Text);
            double Cal4 = Convert.ToDouble(QUIMICA.Text);
            double promedio = (Cal1 + Cal2 + Cal3 + Cal4) / 4;
            PROMEDIO.Text = promedio.ToString();
        }
    }
}
