﻿
namespace P2U2DesPromedio
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MATEMATICAS = new System.Windows.Forms.TextBox();
            this.ESPAÑOL = new System.Windows.Forms.TextBox();
            this.QUIMICA = new System.Windows.Forms.TextBox();
            this.CIENCIAS = new System.Windows.Forms.TextBox();
            this.PROMEDIO = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(165, 298);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 32);
            this.button1.TabIndex = 0;
            this.button1.Text = "CALCULAR PROMEDIO";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "MATEMATICAS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(591, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "ESPAÑOL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(101, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "QUIMICA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(590, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "CIENCIAS NATURALES";
            // 
            // MATEMATICAS
            // 
            this.MATEMATICAS.Location = new System.Drawing.Point(117, 55);
            this.MATEMATICAS.Name = "MATEMATICAS";
            this.MATEMATICAS.Size = new System.Drawing.Size(100, 20);
            this.MATEMATICAS.TabIndex = 5;
            // 
            // ESPAÑOL
            // 
            this.ESPAÑOL.Location = new System.Drawing.Point(593, 55);
            this.ESPAÑOL.Name = "ESPAÑOL";
            this.ESPAÑOL.Size = new System.Drawing.Size(100, 20);
            this.ESPAÑOL.TabIndex = 6;
            // 
            // QUIMICA
            // 
            this.QUIMICA.Location = new System.Drawing.Point(117, 164);
            this.QUIMICA.Name = "QUIMICA";
            this.QUIMICA.Size = new System.Drawing.Size(100, 20);
            this.QUIMICA.TabIndex = 7;
            // 
            // CIENCIAS
            // 
            this.CIENCIAS.Location = new System.Drawing.Point(614, 164);
            this.CIENCIAS.Name = "CIENCIAS";
            this.CIENCIAS.Size = new System.Drawing.Size(100, 20);
            this.CIENCIAS.TabIndex = 8;
            // 
            // PROMEDIO
            // 
            this.PROMEDIO.AutoSize = true;
            this.PROMEDIO.Location = new System.Drawing.Point(474, 298);
            this.PROMEDIO.Name = "PROMEDIO";
            this.PROMEDIO.Size = new System.Drawing.Size(10, 13);
            this.PROMEDIO.TabIndex = 9;
            this.PROMEDIO.Text = ".";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.PROMEDIO);
            this.Controls.Add(this.CIENCIAS);
            this.Controls.Add(this.QUIMICA);
            this.Controls.Add(this.ESPAÑOL);
            this.Controls.Add(this.MATEMATICAS);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox MATEMATICAS;
        private System.Windows.Forms.TextBox ESPAÑOL;
        private System.Windows.Forms.TextBox QUIMICA;
        private System.Windows.Forms.TextBox CIENCIAS;
        private System.Windows.Forms.Label PROMEDIO;
    }
}

