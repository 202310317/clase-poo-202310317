﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void btnCalcularA_Click(object sender, EventArgs e)
        {
            int Largo = int.Parse(txtLargo.Text);
            int Ancho = int.Parse(txtAncho.Text);
            int Radio = int.Parse(txtRadio.Text);
            int Altura = int.Parse(txtAltura.Text);
            Figura Objf = new Figura();
            Objf.CalcularArea(Largo, Ancho, Radio, Altura);

            if (rbRectangulo.Checked == true)
            {
                int X = Ancho * Altura;
                MessageBox.Show("El area de un Rectangulo es: " + X);
            }
            if (rbCircunferencia.Checked == true)
            {
                int Y = (int)(3.1416 * (Radio * 2));
                MessageBox.Show("El area de una Circunferencia es: " + Y);
            }
            if (rbTriangulo.Checked == true)
            {
                int Z = (Ancho * Altura) / 2;
                MessageBox.Show("El area de un Triangulo es: " + Z);
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtLargo_TextChanged(object sender, EventArgs e)
        {

        }

        private void rbRectangulo_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void rbTriangulo_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
