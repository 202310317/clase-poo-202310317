﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1Prog02
{
    class Figura
    {
        public int X; 
        public int Y; 
        public int Z; 

        internal void CalcularArea(int largo, int Ancho, int Radio, int Altura)
        {
            X = Ancho * Altura;
            Y = (int)(3.1416 *(Radio * 2));
            Z = (Ancho * Altura) / 2;
        }
    }
}
