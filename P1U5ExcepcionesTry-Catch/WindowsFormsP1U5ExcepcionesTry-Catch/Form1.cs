﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1U5ExcepcionesTry_Catch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            try
            {
                int num = int.Parse(txtNumero.Text);
                double cuadrado;
                cuadrado = Math.Pow(num, 2);
                lblRes.Text = "Cuadrado= " + cuadrado;
                txtNumero.Text = "";
            }
            catch (OverflowException ex)
            {
                MessageBox.Show("Ha ocurrido un error  " + ex.Message);

            }
            catch (FormatException ex)
            {
                MessageBox.Show("Ha ocurrido un error  "+ ex.Message);

            }
        }

       
    }
}
