﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U4Virtual_Empleado
{
    public class EmpleadoPorComision : Empleado
    {
        public override void daTarifaComision(int T, int V)
        {
            X = T * V;
        }
        
        public override void daVentasBrutas(int T, int V)
        {
            Y = T * V;
        }
    }
}