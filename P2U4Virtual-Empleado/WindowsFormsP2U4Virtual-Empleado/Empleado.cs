﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U4Virtual_Empleado
{
    //virtual-override
    public class Empleado //clase base(padre)
    {
        public int Y; //Salario
        public int X; // Horas
        public int Z; //Salario Base
        public virtual void daSalario(int S, int H)
        {

        }

        public virtual void daHoras(int S, int H)
        {

        }

        public virtual void daSueldo(int S, int H)
        {

        }

        public virtual void daTarifaComision(int T, int V)
        {
            
        }

        public virtual void daVentasBrutas(int T, int V)
        {
            
        }

        public virtual void daSalarioBase(int T, int V, int B)
        {
            
        }
    }
}