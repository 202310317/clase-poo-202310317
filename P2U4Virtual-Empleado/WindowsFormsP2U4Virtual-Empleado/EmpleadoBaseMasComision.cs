﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsP2U4Virtual_Empleado
{
    public class EmpleadoBaseMasComision : Empleado
    {
        public override void daTarifaComision(int T, int V)
        {
            X = 10* 15;
        }
        
        public override void daVentasBrutas(int T, int V)
        {
            Y = 10* 15;
        }
        
        public override void daSalarioBase(int T, int V, int B)
        {
            Z = 10* 15 + B;
        }
    }
}