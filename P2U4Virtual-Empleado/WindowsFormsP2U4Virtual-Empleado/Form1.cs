﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP2U4Virtual_Empleado
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            cboListaEmpleado.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Form fmr = null;
            switch (cboListaEmpleado.SelectedIndex)
            {
                case 0:
                    fmr = new FormEmpleadoAsalariado();
                    break;
                case 1:
                    fmr = new FormEmpleadoPorHoras();
                    break;
                case 2:
                    fmr = new FormEmpleadoPorComision();
                    break;
                case 3:
                    fmr = new FormEmpleadoBaseMasComision();
                    break;
                
            }
            Hide();
            fmr?.ShowDialog();
            Show();
            fmr.Dispose();

        }
    }
}
