﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsP1U5ExcepcionesFinally
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            try
            {
                var num1 = int.Parse(txtNumero.Text);
                var num2 = int.Parse(txtNumero2.Text);
                double resta;
                resta = (num1-num2);
                lblRes.Text = "Resta= " + resta;
                
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.Message);

            }
            catch (InvalidCastException ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                MessageBox.Show("Fin Excepcion");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNumero.Clear();
            txtNumero2.Clear();
           
        }
    }
}
