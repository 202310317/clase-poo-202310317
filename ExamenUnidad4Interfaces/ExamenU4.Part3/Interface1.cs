﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenU4.Part3
{
    interface Vehiculo
    {
        public string Avanzar(string km);
        public int Girar(int g);

    }
}
